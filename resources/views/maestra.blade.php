<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/materialize.css">
    @yield('estilo')
    <style>
    .full{
        width: 90% !important;
    }
    </style>
    <title>@yield('titulo')</title>
</head>
<body>
@yield('nav')
@yield('caro')
<div class="container">
   @yield('contenido')
</div>
    

    <script src="js/jquery-1.12.3.min.js"></script>
    <script src="js/materialize.js"></script>
    <script src="js/script.js"></script>
</body>
</html>