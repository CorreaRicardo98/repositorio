@extends('maestra')
@section('titulo')
    inicio
@endsection
@section('nav')

<nav>
    <div class="nav-wrapper">
      <a href="#" class="brand-logo">Logo</a>
      <ul id="nav-mobile" class="right hide-on-med-and-down">
        <li><a href="{{ route('entrar') }}">Ingresar</a></li>
        <li><a href="badges.html">Carreras</a></li>
        <li><a href="">Ciencias Básicas</a></li>
      </ul>
    </div>
  </nav>
@endsection

@section('caro')
<div class="carousel">
    <a class="carousel-item" href="#one!"><img src="img/carousel/itm1.png"></a>
    <a class="carousel-item" href="#two!"><img src="img/carousel/itm2.png"></a>
    <a class="carousel-item" href="#three!"><img src="img/carousel/itm3.png"></a>
    <a class="carousel-item" href="#four!"><img src="img/carousel/itm4.png"></a>
  </div>
@endsection

@section('contenido')

@endsection