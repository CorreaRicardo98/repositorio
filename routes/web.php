<?php

Route::get('/', 'controller1@inicio');

Route::get('fotos/{numero?}',function($numero = "sin numero"){
    return'Estas en la galeria:'.$numero; 
})->where('numero','[0-9]+');

Route::view('galeria','fotos',['numero'=> 125]);

Route::get('index','controller1@principal');

Route::get('login','controller1@login')->name('entrar');

Route::get('indexA','controller1@principalAuditor')->name('auditorPrincipal');
