<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class controller1 extends Controller
{
    public function inicio(){
        return view('principal');
    }

    public function principal(){
        return view('principal');
    }

    public function login(){
        return view('login');
    }

    public function principalAuditor(){
        return view('auditor_index');
    }

}
